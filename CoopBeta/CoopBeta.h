/*	MPF Coop Beta 4.2.4 with Dragonade 1.8.1 Binaries Code
	Copyright 2012-2016 MPF Games
	https://multiplayerforums.com

	This file is part of the MPF-Games scripts.dll
	The MPF-Games scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

#pragma once

#include "gmplugin.h"

class COOP : public Plugin
{
public:
	struct Configuration
	{
		StringClass ModVersion;
		StringClass ModDate;
		StringClass ModTime;
		int Play_As;
		bool Teleporting_Enabled;

		void setToDefaults()
		{
			ModVersion = "4.2.4-181";
			ModDate = __DATE__;
			ModTime = __TIME__;
			Play_As = 1;
			Teleporting_Enabled = true;
		}
	};

	Configuration globalConfiguration;
	Configuration mapConfiguration;

	COOP();
	~COOP();

	void LoadGenericINISettings(INIClass *SSGMIni, const char* key, Configuration& configuration)
	{
		SSGMIni->Get_String(configuration.ModVersion,key,"ModVersion",configuration.ModVersion);
		SSGMIni->Get_String(configuration.ModDate,key,"ModDate",configuration.ModDate);
		SSGMIni->Get_String(configuration.ModTime,key,"ModTime",configuration.ModTime);

		configuration.Play_As = SSGMIni->Get_Int(key,"Play_As",configuration.Play_As);
	}

	virtual void OnLoadGlobalINISettings(INIClass *SSGMIni);
	virtual void OnLoadMapINISettings(INIClass *SSGMIni);
	virtual void OnObjectCreate(void *data,GameObject *obj);
	virtual bool OnChat(int PlayerID,TextMessageEnum Type,const wchar_t *Message,int recieverID);
};

extern COOP coop;

//script classes below here

class MPF_Teleport_Player_To_Player : public ScriptImpClass {
	void Created(GameObject *obj);
	void Timer_Expired(GameObject *obj, int number);
	GameObject *OwnObject;
	GameObject *GotoObject;
	int OwnObjectID;
	int GotoObjectID;
	Vector3 GotoPos;
	int OwnID;
	int GotoID;
};

class MPF_Player_Cant_Use_Goto : public ScriptImpClass {
	void Created(GameObject *obj);
};

class MPF_Player : public ScriptImpClass {
	void Created(GameObject *obj);
};