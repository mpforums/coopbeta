/*	MPF Coop Beta 4.2.4 with Dragonade 1.8.1 Binaries Code
	Copyright 2012-2016 MPF Games
	https://multiplayerforums.com

	This file is part of the MPF-Games scripts.dll
	The MPF-Games scripts.dll is free software; you can redistribute it and/or modify it under
	the terms of the GNU General Public License as published by the Free
	Software Foundation; either version 2, or (at your option) any later
	version. See the file COPYING for more details.
	In addition, an exemption is given to allow Run Time Dynamic Linking of this code with any closed source module that does not contain code covered by this licence.
	Only the source code to the module(s) containing the licenced code has to be released.
*/

#include "General.h"
#include "engine.h"
#include "engine_da.h"
#include "da_chatcommand.h"
#include "gmgame.h"
#include "gmlog.h"
#include "ScriptableGameObj.h"
#include "GameObjManager.h"
#include "VehicleGameObjDef.h"
#include "VehicleGameObj.h"
#include "SoldierGameObj.h"
#include "BuildingGameObj.h"
#include "BuildingGameObjDef.h"
#include "ArmorWarheadManager.h"
#include "HashTemplateIterator.h"
#include "VehicleFactoryGameObj.h"
#include "PowerUpGameObjDef.h"
#include "GameObjManager.h"
#include "C4GameObj.h"
#include "ScriptManager.h"
#include "SmartGameObj.h"
#include "SmartGameObjDef.h"
#include "GameData.h"
#include "BaseControllerClass.h"
#include "PurchaseSettingsDefClass.h"
#include "CoopBeta.h"

#define PTTEAM(t) (t?0:1)

class DAGotoChatCommandClass : public DAChatCommandClass {
	virtual bool Activate(cPlayer *Player,const DATokenClass &Text,TextMessageEnum ChatType) 
	{
		if (coop.mapConfiguration.Teleporting_Enabled)
		{
			GameObject *Guy = Player->Get_GameObj();
			cPlayer *Move2Guy = Match_Player(Player, Text[1], false,false);
			if (Guy && Move2Guy)
			{
				GameObject *GuyObj = Guy;
				GameObject *Move2GuyObj = Move2Guy->Get_GameObj();

				if (GuyObj != Move2GuyObj)
				{
					if (Commands->Get_Health(GuyObj) > 0.0f && Commands->Get_Health(Move2GuyObj) > 0.0f)
					{
						if (!GuyObj->As_SoldierGameObj()->Is_On_Ladder())
						{
							if (!Is_Script_Attached(GuyObj,"MPF_Player_Cant_Use_Goto"))
							{
								if (!Is_Script_Attached(Move2GuyObj,"JFW_Spy_Switch_Team"))
								{
									Vector3 Move2GuyPos(0,0,0);
									if (GuyObj && Move2GuyObj)
									{
										Move2GuyPos = Commands->Get_Position(Move2GuyObj);
										Commands->Set_Position(GuyObj, Move2GuyPos);
										Fix_Stuck_Object(GuyObj->As_PhysicalGameObj(),15.0f);
										Send_Message_Player(GuyObj,255,255,255,StringClass::getFormattedString("[MPF] You were moved to %s's position.",Get_Player_Name(Move2GuyObj)));
										Send_Message_Player(Move2GuyObj,255,255,255,StringClass::getFormattedString("[MPF] %s was moved to your position.",Get_Player_Name(GuyObj)));										
									}
									else
									{
										Send_Message_Player(Player->Get_GameObj(), 255,255,255, "[MPF] Could not teleport because the players GameObj's could not be located!");
									}
								}
								else
								{
									Send_Message_Player(GuyObj,255,255,255,"[MPF] You cannot use !goto on other spies.");
								}
							}
							else
							{
								Send_Message_Player(GuyObj,255,255,255,"[MPF] You cannot use !goto because your character appears to be restricted.");
							}
						}
						else
						{
							Send_Message_Player(GuyObj,255,255,255,"[MPF] You cannot use !goto because you are on a ladder.");
						}
					}
					else
					{
						Send_Message_Player(GuyObj,255,255,255,"[MPF] You cannot use !goto because one of you is dead.");
					}
				}
				else
				{
					Send_Message_Player(GuyObj,255,255,255,"[MPF] You cannot use !goto to yourself noob.");
				}
			}
			else
			{
				if (Guy)
				{
					Send_Message_Player(Guy,255,255,255,"[MPF] ERROR: The player to move, or the player to goto, was not found.");
				}
				if (Move2Guy)
				{
					Send_Message_Player(Move2Guy->Get_GameObj(),255,255,255,"[MPF] ERROR: The player to move, or the player to goto, was not found.");
				}
			}
		}
		else
		{
			Send_Message(255,0,220,"[MPF] Sorry: Goto is disabled on this map.");
		}
		return true;
	}
};
Register_Full_Chat_Command(DAGotoChatCommandClass, "!go|!goto", 1, DAAccessLevel::NONE, DAChatType::ALL);



COOP::COOP()
{
	RegisterEvent(EVENT_CHAT_HOOK,this);
	RegisterEvent(EVENT_GLOBAL_INI,this);
	RegisterEvent(EVENT_MAP_INI,this);
	RegisterEvent(EVENT_OBJECT_CREATE_HOOK,this);
}
COOP::~COOP()
{
	UnregisterEvent(EVENT_CHAT_HOOK,this);
	UnregisterEvent(EVENT_GLOBAL_INI,this);
	UnregisterEvent(EVENT_MAP_INI,this);
	UnregisterEvent(EVENT_OBJECT_CREATE_HOOK,this);
}

void COOP::OnLoadGlobalINISettings(INIClass *SSGMIni)
{
	globalConfiguration.setToDefaults();
	LoadGenericINISettings(SSGMIni, "CoopBeta", globalConfiguration);
}

void COOP::OnLoadMapINISettings(INIClass *SSGMIni)
{
	mapConfiguration = globalConfiguration;
	const StringClass key = StringClass(The_Game()->Get_Map_Name()).AsLower() + "_Coop";
	LoadGenericINISettings(SSGMIni, key, mapConfiguration);

	mapConfiguration.Play_As = SSGMIni->Get_Int(key,"Play_As",mapConfiguration.Play_As);
	mapConfiguration.Teleporting_Enabled = SSGMIni->Get_Bool(key,"Teleporting_Enabled",mapConfiguration.Teleporting_Enabled);
}

void COOP::OnObjectCreate(void *data,GameObject *obj)
{
	if (obj && obj->As_SoldierGameObj())
	{
		if (Commands->Is_A_Star(obj))
		{
			Attach_Script_Once(obj,"MPF_Player","");
		}
	}
}

bool COOP::OnChat(int PlayerID,TextMessageEnum Type,const wchar_t *Message,int recieverID)
{
	if (Message[0] == L'!')
	{
		if (wcsistr(Message,L"!modinfo") == Message)
		{
			Console_Input("msg This server is running zunnie's Coop Beta 4.2.4 with Dragonade 1.8.1 Support.");
			char modinfo[512];
			sprintf(modinfo,"msg Current version: %s - Last Update: %s @ %s",coop.mapConfiguration.ModVersion,coop.mapConfiguration.ModDate,mapConfiguration.ModTime);
			Console_Input(modinfo);
			return false;
		}

		if (wcsistr(Message,L"!map") == Message)
		{
			Console_Input(StringClass::getFormattedString("msg The current map is %s", The_Cnc_Game()->Get_Map_Name()));
			return false;
		}
	}
	return true;
}

COOP coop;

extern "C" __declspec(dllexport) Plugin* Plugin_Init()
{
	return &coop;
}

//scripts code below here

void MPF_Player_Cant_Use_Goto::Created(GameObject *obj) 
{ 
	//Dummy script for use when someone cannot use the goto/go command.
}
ScriptRegistrant<MPF_Player_Cant_Use_Goto> MPF_Player_Cant_Use_Goto_Registrant("MPF_Player_Cant_Use_Goto","");

void MPF_Player::Created(GameObject *obj)
{
	if (Get_Object_Type(obj) != coop.mapConfiguration.Play_As)
	{
		Change_Team(obj,coop.mapConfiguration.Play_As);
	}
}
ScriptRegistrant<MPF_Player> MPF_Player_Registrant("MPF_Player","");